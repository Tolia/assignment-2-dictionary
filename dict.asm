%include "lib.inc"
%define POINTER_SIZE 8

global word_find

section .text

word_find:
    xor rax, rax
    cmp rsi, 0
    je .fail
    add rsi, POINTER_SIZE
    mov r10,rdi
    mov r11,rsi
    call string_equals
    mov rdi,r10
    mov rsi,r11
    test rax, rax
    jnz  .success
    mov rsi, [rsi - POINTER_SIZE]
    jmp word_find

.success:
    sub rsi, POINTER_SIZE
    mov rax, rsi
    ret

.fail:
    xor rax, rax
    ret
